package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.myapplication.R.id.txtResultado

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val campoNomeUsuario = findViewById<EditText>(R.id.edtNomeUsuario)
        val botaoExibirMensagem = findViewById<Button>(R.id.btnExibirMensagem)
        val textoMensagemExibida = findViewById<TextView>(R.id.txtResultado)



        botaoExibirMensagem.setOnClickListener {
            var nomeUsuario = campoNomeUsuario.text.toString()

            nomeUsuario?.let {
                textoMensagemExibida.text = "Boas vindas, $nomeUsuario"
            }
        }

    }
}